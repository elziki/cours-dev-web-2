# Frontend Web Development

Quentin Richaud

qrichaud.pro@gmail.com

---

# CSS Preprocessor

CSS is a limited language, lacking flexibility.

However it is the only stylesheet language that is understood by all web browser.

To overcome this solution, we have invented other stylesheet languages, which we use
to write our stylesheets, and that needs to be translated into CSS in order to send
them to the browser. That is **CSS preprocessors**.

---

# Sass and Scss

SASS is one of these languages. It has 2 syntaxes : 

- SASS proper, which inspires it syntax from ruby (it was invented to be used with the backend
  framework Ruby on Rails)
- SCSS : which as the same feature and logic than Sass, but uses a syntax close to original CSS.

In this lecture we will see SCSS.

Other examples of CSS preprocessors : Less, Stylus, PostCSS to name a few.

---

# Example of an SCSS source

```scss
.page-content {
    padding-top: 20px;

    .title {
        color: red;
    }

    .menu {
        background-color: green;
    }
}
```

Here the nested syntax is typical of SCSS, it isn't valid CSS. The corresponding CSS source would be :

```css
.page-content {
  padding-top: 20px; 
}
.page-content .title {
  color: red; 
}
.page-content .menu {
  background-color: green; 
}
```

---

# Installing SASS to transpile your sources

Sass is a NodeJS program. We will see NodeJS later in the javascript lecture. For now, just follow
the installation instruction.

1. You need to have nodeJS and npm (the package manager for nodeJS) installed.

Installation instuctions depend on your system. For ubuntu/debian : 

```
# Add node software repository
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
# Install node (version 18) and npm
sudo apt install  nodejs
```

You can test that the installation was successfull by checking the version number of both executables :

```
$ node --version
v12.22.12
$ npm --version
6.14.16
```

---

# Installing SASS to transpile your sources

Sass is a NodeJS program. We will see NodeJS later in the javascript lecture. For now, just follow
the installation instruction.

2. Install the npm package for sass (globally)

```
$ sudo npm install -g sass
```

NPM is the node package manager, it can install packages locally (for the current project only), 
or globally (package will be available on the whole system). It is akin to `pip` for python.

In this demo, it is easier to work with a global installation of Sass (otherwise, we would need
to configure a npm project, which is not in the frame of this lecture).

For a global installation, you will need administrator rights (use `sudo`).

---

# Installing SASS to transpile your sources

3. You can now use Sass to transpile a SCSS source to a CSS source

Example :

```
$ sass style.scss style.css
```

---

# Installing SASS to transpile you sources

Running the command : 

```
sass --watch test.scss test.css
```

will automatically transpile the source again each time it changes.

---

# SCSS syntax, a few examples : Nesting

Nesting selectors, which makes the source more readable. Examples from the demo `mines_lecole_mockup` 
from previous lecture.

---
CSS : 

```css
.title-banner .header-content {
    width: 1140px;
    margin-right: auto;
    margin-left: auto;
    display: flex;
    color: white;
}

.title-banner .header-content .left-placeholder {
    width: 190px;
    background-color: #61259e;
}

.title-banner .header-content .right-header-content {
    padding: 40px;

}

.title-banner .header-content .right-header-content h1 {
    font-size: 40px;
    font-weight: 400;
    margin-bottom: 20px;
}

.title-banner .header-content .right-header-content .subtitle-menu {
    font-size: 12px;
}
```

---

SCSS 

```scss
.title-banner .header-content {
    width: 1140px;
    margin-right: auto;
    margin-left: auto;
    display: flex;
    color: white;


    .left-placeholder {
        width: 190px;
        background-color: #61259e;
    }

    .right-header-content {
        padding: 40px;

        h1 {
            font-size: 40px;
            font-weight: 400;
            margin-bottom: 20px;
        }

        .subtitle-menu {
            font-size: 12px;
        }
    }
}
```

---

Another example of nesting. 

CSS :

```css
.title-banner .header-content .right-header-content .subtitle-menu .menu-item.active,
.title-banner .header-content .right-header-content .subtitle-menu .menu-item:hover,
.title-banner .header-content .right-header-content .subtitle-menu .delimiter {
    color: #A26CDA;
}
```

SCSS :

```scss
.title-banner .header-content .right-header-content .subtitle-menu {
    .menu-item.active,
    .menu-item:hover,
    .delimiter {
        color: #A26CDA;
    }
}
```

---

# SCSS syntax example : using `&` in nesting

When nesting, the `&` sign can be used to invoke the parent selector, in order
to combine it in the child selector (pairing with another classname, or using a pseudo selector
for example).

CSS :

```css
.menu-link {
    font-size: 14px;
}

.menu-link.active,
.menu-link:hover {
    text-decoration: underline;
}
```

SCSS : 

```scss
.menu-link {
    font-size: 14px;

    &.active,
    &:hover {
        text-decoration: underline;
    }
}
```

---

# SCSS syntax example : variables

CSS :

```css
.top-header .nav-menu .menu-item.active,
.top-header .nav-menu .menu-item:hover {
    color: #61259e;
}

.title-banner .header-content .left-placeholder {
    width: 190px;
    background-color: #61259e;
}
```

SCSS : 

```scss
$dark-purple: ##61259e;

.top-header .nav-menu .menu-item.active,
.top-header .nav-menu .menu-item:hover {
    color: $dark-purple;
}

.title-banner .header-content .left-placeholder {
    width: 190px;
    background-color: $dark-purple;
}
```


---

# SCSS syntax example : modules

Allows to split the code into several files, and import them.

Example :

`variables.scss`

```scss
// colors
$dark-purple: ##61259e;
$dark-gray: #333333;

// layout sizes
$default-gutter-size: 20px;
```

`style.scss`

```scss
@use 'variables.scss' as vars;

.title {
    color: vars.$dark-gray;
    margin-bottom: vars.$default-gutter-size;
}

```

---

# Demo

You can check the folders `demo_code/mines_lecole_mockup_with_SCSS`, which is a refactored
implementation of the demo of the first lecture, with the stylesheet written in SCSS. 

You will need to run sass in order to compile the SCSS source. You can use the provide script
`compile_css.sh`.